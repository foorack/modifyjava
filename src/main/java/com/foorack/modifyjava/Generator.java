package com.foorack.modifyjava;

import lombok.*;
import java.util.function.*;
import com.github.javaparser.ast.stmt.*;

@Data
public class Generator<T> {
    public final int weight;
    public final Function<Environment, T> function;
}
