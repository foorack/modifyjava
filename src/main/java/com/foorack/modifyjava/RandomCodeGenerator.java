package com.foorack.modifyjava;

import java.lang.reflect.Constructor;
import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;
import java.util.function.*;

import com.github.javaparser.*;
import com.github.javaparser.ast.*;
import com.github.javaparser.ast.visitor.*;
import com.github.javaparser.ast.body.*;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.type.*;
import com.github.javaparser.metamodel.*;
import com.github.javaparser.printer.*;
import com.github.javaparser.resolution.types.*;
import com.github.javaparser.symbolsolver.*;
import com.github.javaparser.utils.*;
import com.github.javaparser.symbolsolver.*;
import com.github.javaparser.symbolsolver.resolution.typesolvers.*;
import com.github.javaparser.symbolsolver.javaparsermodel.*;
import com.github.javaparser.symbolsolver.model.resolution.*;

import static com.foorack.modifyjava.ModifyJava.*;

import lombok.*;

public class RandomCodeGenerator {

    @SneakyThrows
    public RandomCodeGenerator() {
// Symbolsolver
        CombinedTypeSolver combinedTypeSolver = new CombinedTypeSolver();
        combinedTypeSolver.add(new ReflectionTypeSolver());
        //Files.walk(Paths.get("/home/foorack/.m2/repository/")).map(Path::toFile).filter(File::isFile).filter(f -> f.getName().endsWith(".jar")).forEach(f -> {
        //    try {
        //        combinedTypeSolver.add(new JarTypeSolver(f));
        //    } catch(Exception e) {
        //        e.printStackTrace();
        //    }
        //});
        //Files.walk(Paths.get("/home/foorack/.m2/repository/")).map(Path::toFile).filter(File::isFile).filter(f -> f.getName().endsWith(".jar")).forEach(f -> System.out.println(f.getAbsolutePath()));
        //System.out.println();
        //System.out.println();
        //System.out.println();
        JavaSymbolSolver symbolSolver = new JavaSymbolSolver(combinedTypeSolver);
        JavaParser.getStaticConfiguration().setSymbolResolver(symbolSolver);


        generateCode();
    }

    public Map<String, CompilationUnit> indexJRE(TypeSolver typeSolver) {
        // Index jre
        Map<String, CompilationUnit> jre = new HashMap<>();
        SourceRoot jresource = new SourceRoot(Paths.get("/usr/lib/jvm/openjdk-11/lib/src/java.base/"));
        List<ParseResult<CompilationUnit>> results = jresource.tryToParseParallelized("java.util");
        results.forEach(result -> {
            CompilationUnit cu = result.getResult().get();
            String name = cu.getPrimaryTypeName().get();
            String path = getFullPackageName(cu.getPackageDeclaration().get()) + "." + name;
            if(!name.equals("package-info") && !name.equals("module-info")) {
                System.out.println("Loaded: " + path);
                jre.put(path, cu);
            }
        });
        System.out.println("a " + jre.size());
        Random r = new Random();
        CompilationUnit ru = jre.get(new ArrayList<String>(jre.keySet()).get(r.nextInt(jre.size())));
        System.out.println("d " + getFullPackageName(ru.getPackageDeclaration().get()) + "." + ru.getPrimaryTypeName().get());

        for(BodyDeclaration<?> member : ru.getType(0).getMembers()) {
            if (!member.isMethodDeclaration()) {
                continue;
            }
            MethodDeclaration method = member.asMethodDeclaration();
            NodeList<Parameter> parameters = method.getParameters();
            if(parameters.size() == 0) {
                continue;
            }
            System.out.println(method.getName());
            for(Parameter p : parameters) {
                ResolvedType rt = JavaParserFacade.get(typeSolver).convertToUsage(p.getType());
                if(rt.isReferenceType()) {
                    System.out.println("-   " + p.getName() + " " + rt.describe() + " " + rt.asReferenceType().getQualifiedName());
                } else {
                    System.out.println("    " + p.getName() + " " + rt.describe());
                }
            }
        }
        return jre;
    }

    @SneakyThrows
    public void generateCode() {
        // Cleanup from previous
        if(new File("output").isDirectory()) {
            Files.walk(Paths.get("output")).map(Path::toFile).sorted((o1, o2) -> -o1.compareTo(o2)).forEach(File::delete);
        }

        // Create maven folder structure
        new File("output/src/main/java/generated").mkdirs();
        Files.copy(RandomCodeGenerator.class.getResourceAsStream("/pom.xml"), FileSystems.getDefault().getPath("output", "pom.xml"));

        // Create main class
        CompilationUnit cu = createUnit(new File("output/src/main/java"), "generated", "RandomCodeGenerated");
        ClassOrInterfaceDeclaration c = cu.addClass("RandomCodeGenerated");
        c.addMember(new MethodDeclaration(EnumSet.of(Modifier.PUBLIC, Modifier.STATIC), "main", new VoidType(), new NodeList<Parameter>(new Parameter(new ArrayType(new ClassOrInterfaceType(null, "String")), "args"))));
        MethodDeclaration main = getMethodDeclarations(cu, "main").get(0);
        BlockStmt mainb = new BlockStmt();

        // Environment setup
        Environment env = new Environment();

        // Add statements
        generateStmts(env).forEach(s -> mainb.addStatement(s));

        main.setBody(mainb);
        saveUnit(cu);
        //YamlPrinter printer = new YamlPrinter(true);
        //System.out.println(printer.output(cu));
    }

    private NodeList<Statement> generateStmts(final @NonNull Environment env) {
        NodeList<Statement> stmts = new NodeList<>();
        int limit = env.random().nextInt(200) + 100;
        for(int i = 0; i != limit; i++) {
            Statement stmt = generateStmt(env);
            stmts.add(stmt);
            if(stmt.isReturnStmt() || stmt.isBreakStmt() || stmt.isContinueStmt()) {
                break;
            }
        }
        return stmts;
    }

    private Statement generateStmt(final @NonNull Environment env) {
        List<Generator<Statement>> generators = new ArrayList<>();

        LinkedList<Integer> ws = new LinkedList<>(List.of(20, 20, 40, 8, 4));

        //  Break Stmt
        generators.add(new Generator<Statement>(ws.remove(), (e) -> {
            if(e.isInsideLoop()) {
                return new BreakStmt(new SimpleName(e.getLoopLabels().get(e.random().nextInt(e.getLoopLabels().size()))));
            } else {
                return null;
            }
        }));
        // Continue Stmt
        generators.add(new Generator<Statement>(ws.remove(), (e) -> {
            if(e.isInsideLoop()) {
                return new ContinueStmt(new SimpleName(e.getLoopLabels().get(e.random().nextInt(e.getLoopLabels().size()))));
            } else {
                return null;
            }
        }));
        // Primitive Assign Expr
        generators.add(new Generator<Statement>(ws.remove(), (e) -> {
            if(e.getScopedVariables().size() == 0) {
                return null;
            }
            String vname = new ArrayList<>(e.getScopedVariables().keySet()).get(e.random().nextInt(e.getScopedVariables().size()));
            Type type = e.getScopedVariables().get(vname);

            List<AssignExpr.Operator> ops = new ArrayList<AssignExpr.Operator>(Arrays.asList(AssignExpr.Operator.values()));
            if(type.isPrimitiveType() && type.asPrimitiveType().getType().equals(PrimitiveType.Primitive.BOOLEAN)) {
                ops = List.of(AssignExpr.Operator.ASSIGN);
            }
            if(type.isPrimitiveType() && (type.asPrimitiveType().getType().equals(PrimitiveType.Primitive.FLOAT)
                                          || type.asPrimitiveType().getType().equals(PrimitiveType.Primitive.DOUBLE))) {
                ops.remove(AssignExpr.Operator.BINARY_AND);
                ops.remove(AssignExpr.Operator.BINARY_OR);
                ops.remove(AssignExpr.Operator.LEFT_SHIFT);
                ops.remove(AssignExpr.Operator.SIGNED_RIGHT_SHIFT);
                ops.remove(AssignExpr.Operator.UNSIGNED_RIGHT_SHIFT);
                ops.remove(AssignExpr.Operator.XOR);
            }
            AssignExpr.Operator operator = ops.get(e.random().nextInt(ops.size()));

            List<String> countSame = new ArrayList<>();
            for(String oname : e.getScopedVariables().keySet()) {
                if(type.equals(e.getScopedVariables().get(oname)) && !vname.equals(oname)) {
                    countSame.add(oname);
                }
            }

            if(countSame.size() == 0) { // only self
                Expression value = null;
                if(type.isPrimitiveType() && (operator.equals(AssignExpr.Operator.LEFT_SHIFT) || operator.equals(AssignExpr.Operator.SIGNED_RIGHT_SHIFT) || operator.equals(AssignExpr.Operator.UNSIGNED_RIGHT_SHIFT))) {
                    PrimitiveType ptype = type.asPrimitiveType();
                    if(ptype.getType().equals(PrimitiveType.Primitive.BYTE)) {
                        value = generateValueExpr(e, new PrimitiveType(PrimitiveType.Primitive.BYTE));
                    } else if(ptype.getType().equals(PrimitiveType.Primitive.SHORT)) {
                        value = generateValueExpr(e, new PrimitiveType(PrimitiveType.Primitive.SHORT));
                    } else {
                        value = generateValueExpr(e, new PrimitiveType(PrimitiveType.Primitive.INT));
                    }
                } else {
                    value = generateValueExpr(e, type);
                }

                return new ExpressionStmt(new AssignExpr(new NameExpr(vname), value, operator));
            } else {
                String oname = countSame.get(e.random().nextInt(countSame.size()));
                return new ExpressionStmt(new AssignExpr(new NameExpr(vname), new NameExpr(oname), operator));
            }
        }));
        // Primitive VariableDeclaration Expr
        generators.add(new Generator<Statement>(ws.remove(), (e) -> {
            String name = Environment.generateVariableName();
            Type type = new PrimitiveType(PrimitiveType.Primitive.values()[e.random().nextInt(PrimitiveType.Primitive.values().length)]);
            e.addScopedVariable(name, type);
            return new ExpressionStmt(new VariableDeclarationExpr(new VariableDeclarator(type, name, generateValueExpr(e, type))));
        }));
        // While Stmt
        generators.add(new Generator<Statement>(ws.remove(), (e) -> {
            String label = Environment.generateLoopLabel();
            return new LabeledStmt(label, new WhileStmt(generateValueExpr(e, new PrimitiveType(PrimitiveType.Primitive.BOOLEAN)),
                                   new BlockStmt(generateStmts(e.setInsideLoop(true).addLoopLabel(label)))));
        }));

        while(true) {
            List<Function<Environment, Statement>> weightedRandom = new ArrayList<>();
            for(Generator<Statement> sg : generators) {
                for(int i = 0; i != sg.getWeight(); i++) {
                    weightedRandom.add(sg.getFunction());
                }
            }
            Function<Environment, Statement> generator = weightedRandom.get(env.random().nextInt(weightedRandom.size()));
            Statement stmt = generator.apply(env);
            if (stmt != null) {
                return stmt;
            }
        }
    }

    private Expression generateValueExpr(final @NonNull Environment env, final @NonNull Type type) {
        List<Generator<Expression>> generators = new ArrayList<>();

        LinkedList<Integer> ws = new LinkedList<>(List.of(10));

        // Random primitive value generator
        generators.add(new Generator<Expression>(ws.remove(), (e) -> {
            if(!type.isPrimitiveType()) {
                return null;
            }
            PrimitiveType pt = type.asPrimitiveType();
            System.out.println(pt.getType());
            if(pt.getType().equals(PrimitiveType.Primitive.BOOLEAN)) {
                return new BooleanLiteralExpr(true); // FIXME nextBoolean
            }
            if(pt.getType().equals(PrimitiveType.Primitive.BYTE)) {
                byte[] b = new byte[1];
                e.random().nextBytes(b);
                return new IntegerLiteralExpr((int) b[0]);
            }
            if(pt.getType().equals(PrimitiveType.Primitive.CHAR)) {
                return new IntegerLiteralExpr((char) (e.random().nextInt(Short.MIN_VALUE * -2) + Short.MIN_VALUE));
            }
            if(pt.getType().equals(PrimitiveType.Primitive.DOUBLE)) {
                return new DoubleLiteralExpr(((double) ((((long) e.random().nextInt()) << 32) | e.random().nextInt())) + "D");
            }
            if(pt.getType().equals(PrimitiveType.Primitive.FLOAT)) {
                return new DoubleLiteralExpr((float) e.random().nextInt() + "F");
            }
            if(pt.getType().equals(PrimitiveType.Primitive.INT)) {
                return new IntegerLiteralExpr(e.random().nextInt());
            }
            if(pt.getType().equals(PrimitiveType.Primitive.LONG)) {
                return new LongLiteralExpr(((((long) e.random().nextInt()) << 32) | e.random().nextInt()) + "L");
            }
            if(pt.getType().equals(PrimitiveType.Primitive.SHORT)) {
                return new IntegerLiteralExpr((short) (e.random().nextInt(Short.MIN_VALUE * -2) + Short.MIN_VALUE));
            }
            throw new UnsupportedOperationException("Unknown primitve type.");
        }));

        while(true) {
            List<Function<Environment, Expression>> weightedRandom = new ArrayList<>();
            for(Generator<Expression> sg : generators) {
                for(int i = 0; i != sg.getWeight(); i++) {
                    weightedRandom.add(sg.getFunction());
                }
            }
            Function<Environment, Expression> generator = weightedRandom.get(env.random().nextInt(weightedRandom.size()));
            Expression expr = generator.apply(env);
            if (expr != null) {
                return expr;
            }
        }
    }
}
