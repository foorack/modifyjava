package com.foorack.modifyjava;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.function.*;

import com.github.javaparser.*;
import com.github.javaparser.ast.*;
import com.github.javaparser.ast.visitor.*;
import com.github.javaparser.ast.body.*;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.type.*;
import com.github.javaparser.printer.*;

import lombok.*;

public class ModifyJava {

    public static void main(String[] args) {
        new RandomCodeGenerator();
    }

    public ModifyJava() {
    }

    /**
    * This method returns all files in a directory, including inside sub-directories.
    * This will only returns files and not directories.
    *
    * @param file Folder to search in
    * @return List of all files and sub-files in all sub-directories.
    */
    public static List<File> listAllFiles(File dir) {
        ArrayList<File> files = new ArrayList<>();
        for(File file : dir.listFiles()) {
            if(file.isDirectory()) {
                files.addAll(listAllFiles(file));
            } else {
                files.add(file);
            }
        }
        return files;
    }

    /**
    * Searches for and returns a specific java Unit by the given name.
    * A unit is a compilable file.
    *
    * @param dir Path to search within
    * @param name Name of unit to search for
    * @return Unit with the matched name, or null if not found
    */
    private static CompilationUnit getUnit(File dir, String name) {
        for(File file : listAllFiles(dir)) {
            if (!file.getName().equals(name + ".java")) {
                continue;
            }
            return getUnitFromFile(file);
        }
        return null;
    }

    @SneakyThrows
    public static CompilationUnit getUnitFromFile(File file) {
        return JavaParser.parse(file);
    }

    @SneakyThrows
    public static void saveUnit(CompilationUnit unit) {
        String code = new PrettyPrinter().print(unit);
        Files.write(unit.getStorage().get().getPath(), code.getBytes(com.github.javaparser.Providers.UTF8));
    }

    /**
    * Searches for and returns all methods found with the given name.
    *
    * @param cu CompilationUnit to search in
    * @param name Name of method to find
    * @return List of methods with the given name, empty list if none found
    */
    public static List<MethodDeclaration> getMethodDeclarations(CompilationUnit cu, String name) {
        List<MethodDeclaration> decls = new ArrayList<>();
        for(BodyDeclaration<?> member : cu.getType(0).getMembers()) {
            if (!member.isMethodDeclaration()) {
                continue;
            }
            MethodDeclaration method = member.asMethodDeclaration();
            if(method.getName().getIdentifier().equals(name)) {
                decls.add(method);
            }
        }
        return decls;
    }

    /**
    * Package names are denoted by a tree of qualifer-Name objects. This resolves the whole tree and returns the full path.
    * @param package Package declaration to process.
    * @return Full package path, for example "java.util.stream"
    */
    public static String getFullPackageName(PackageDeclaration pd) {
        Name n = pd.getName();
        String full = n.getIdentifier();
        while(n.getQualifier().isPresent()) {
            n = n.getQualifier().get();
            full = n.getIdentifier() + "." + full;
        }
        return full;
    }

    /**
     * Creates a new CompilationUnit with the given path, package and class name.
     * @param path Path of where to create the file.
     * @param p Fully qualified.package.declaration
     * @String n Name of class/file to create.
    */
    public static CompilationUnit createUnit(File path, String p, String n) {
        CompilationUnit cu = new CompilationUnit(p);
        cu.setStorage(new File(new File(path, p.replaceAll("\\.", File.separator)), n + ".java").toPath());
        return cu;
    }

}
