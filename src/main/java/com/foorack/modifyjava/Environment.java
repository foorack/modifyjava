package com.foorack.modifyjava;

import java.util.*;
import lombok.*;
import lombok.experimental.*;
import com.github.javaparser.ast.type.*;

public class Environment {

    // STATIC PART
    private static final List<String> NAMESPACE = new ArrayList<>();

    public static String generateNamespace(@NonNull String prefix) {
        int i = 1;
        while(true) {
            if(!NAMESPACE.contains(prefix + i)) {
                NAMESPACE.add(prefix + i);
                return prefix + i;
            }
            i++;
        }
    }
    public static String generateLoopLabel() {
        return generateNamespace("l");
    }
    public static String generateVariableName() {
        return generateNamespace("v");
    }

    @Accessors(fluent=true)
    @Getter private static final Random random = new Random();

    // LOCAL PART

    @Getter private boolean insideLoop = false;
    @Getter private List<String> loopLabels = Collections.unmodifiableList(new ArrayList<>());
    @Getter private Map<String, Type> scopedVariables = new HashMap<>();

    public Environment clone() {
        Environment e = new Environment();
        e.insideLoop = insideLoop;
        e.loopLabels = loopLabels; // unmodifiable so no need to copy
        e.scopedVariables = new HashMap<>(scopedVariables);
        return e;
    }

    public void addScopedVariable(String name, Type type) {
        // do not create a new clone, we want this to be passed to parent
        scopedVariables.put(name, type);
    }

    public Environment addLoopLabel(String s) {
        Environment e = clone();
        List<String> l = new ArrayList<>();
        l.addAll(e.loopLabels);
        l.add(s);
        e.loopLabels = Collections.unmodifiableList(l);
        return e;
    }

    public Environment setInsideLoop(boolean v) {
        Environment e = clone();
        e.insideLoop = true;
        return e;
    }

}
